#!/bin/bash
mkdir $HOME/.ptorrent &&
webtorrent --mpv -o $HOME/.ptorrent $@ &&
rm -rf $HOME/.ptorrent 